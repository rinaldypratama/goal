var express = require('express');
var router = express.Router();
var soal = require('../model/soal_model');
var auth = require('../auth.js');
var array;

//get soal by Id, need variable "id mata pelajaran & soal"
router.get('/id/:mapel&:soal?', auth.verifyToken, function(req,res,next){
	soal.getSoalbyId(req.params, function(err,rows){
		if(err)
		{
			res.json(err);
		}
		else
		{
			res.json(rows);
		}
	})
});

//get All soal by mata pelajaran, need variable "id mata pelajaran"
router.get('/mapel/:mapel', auth.verifyToken, function(req,res,next){
	soal.getAllSoal(req.params.mapel, function(err,rows){
		if(err)
		{
			res.json(err);
		}
		else
		{
			res.json(rows);
		}
	})
});

//get All soal by user, need variable "id user and id mata pelajaran"
router.get('/user/:user&:mapel', auth.verifyToken, function(req,res,next){
	soal.getSoalbyUser(req.params, function(err,rows){
		if(err)
		{
			res.json(err);
		}
		else
		{
			res.json(rows);
		}
	})
});

//insert soal
router.post('/', auth.verifyToken, function(req,res,next){
	soal.addSoal(req.body,function(err,count){
		if(err)
		{
			res.json(err);
		}
		else{
			res.json(req.body);
		}
	})
});

//update soal
router.put('/:id', auth.verifyToken, function(req,res,next){
	soal.updateSoal(req.params.id,req.body,function(err,rows){
		if(err)
		{
			res.json(err);
		}
		else
		{
			res.json(rows);
		}
	})
});

//delete soal
router.delete('/:id', auth.verifyToken, function(req,res,next){
	soal.deleteSoal(req.params.id,function(err,rows){
		if(err) 
		{
			res.json(err);
		}
		else
		{
			res.json(count);
		}
	})
});

module.exports = router;