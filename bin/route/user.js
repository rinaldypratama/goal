var express = require('express');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var router = express.Router();
var db = require('../dbconnection');
var auth = require('../auth.js')

function createToken(user) {
	return jwt.sign(user, auth.secret, { expiresIn: 60*60*5 });
}

//get All user list
router.get('/all/', auth.verifyToken, function(req,res,next){
	db.query("select * from user",function(err, rows, fields){
		if(err)
		{
			res.json(err)
		}
		else
		{
			res.json(rows);
		}
	});
});

//get user list by id or email, need parameter id or email user
router.get('/:param?', auth.verifyToken, function(req,res,next){
	db.query("select * from user where id_user=?",[req.params.param],function(err,rows){
		if(rows.length > 0)
		{
			res.json(rows);
		}
		else 
		{
			db.query("select * from user where email=?",[req.params.param],function(err,rows){
				if(err)
				{
					res.json(err)
				}
				else
				{
					res.json(rows);
				}
			});
		}
	});
});

//update user by ID or email, need parameter id or email user
router.put('/:param', auth.verifyToken, function(req,res,next){
	db.getConnection(function(err,connection){
		connection.query("select * from user where id_user=?",[req.params.param],function(err,rows){
			if(rows.length > 0)
			{
				var hashedPassword = bcrypt.hashSync(req.body.password, 8);
				connection.query("update user set first_name=?, last_name=?, email=?, password=?, avatar=? where id_user=?",
				[req.body.first_name,req.body.last_name,req.body.email,hashedPassword,req.body.avatar,req.params.param],function(err,results){
					if(err)
					{
						res.json(err)
					}
					else
					{
						res.status(201).send("Data user berhasil dirubah");
					}
				});
			}
			else
			{
				connection.query("select * from user where email=?",[req.params.param],function(err,rows){
					if(rows.length > 0)
					{
						var hashedPassword = bcrypt.hashSync(req.body.password, 8);
						connection.query("update user set first_name=?, last_name=?, email=?, password=?, avatar=? where email=?",
						[req.body.first_name,req.body.last_name,req.body.email,hashedPassword,req.body.avatar,req.params.param],function(err,results){
							if(err)
							{
								res.json(err)
							}
							else
							{
								res.status(201).send("Data user berhasil dirubah");
							}
						});
					}
					else
					{
						res.status(400).send("id ataupun email user tidak ditemukan");
					}
				});
			}
		});
	});
});

//delete user by ID, need parameter user id
router.delete('/:param', auth.verifyToken, function(req,res,next){
	db.query("delete from user where id_user=?",[req.params.param],function(err,results){
		if(results.affectedRows != 0)
		{
			res.status(200).send("Data user berhasil dihapus");
		}
		else
		{
			db.query("delete from user where email=?",[req.params.param],function(err,results){
				if(results.affectedRows != 0)
				{
					res.status(200).send("Data user berhasil dihapus");
				}
				else
				{
					res.status(400).send("id ataupun email user tidak ditemukan");
				}
			});
		}
	});
});

//create user
router.post('/signup',function(req,res,next){
	if (!req.body.email || !req.body.password) {
		return res.status(400).send("Mohon masukkan email dan password!");
	} else {
		db.getConnection(function(err,connection){
			connection.query("select * from user where email=?",[req.body.email],function(err,rows){
				if(rows.length > 0)
				{
					res.status(400).send("Alamat email telah digunakan");
				}
				else
				{
					var hashedPassword = bcrypt.hashSync(req.body.password, 8);

					connection.query("insert into user(first_name,last_name,email,password,avatar) values(?,?,?,?,?)",
					[req.body.first_name,req.body.last_name,req.body.email,hashedPassword,req.body.avatar],function(err,results){
						if(err)
						{
							res.json(err);
						}
						else
						{
							newUser = {
								id: results.insertId,
								email: req.body.email,
								first_name: req.body.first_name,
								last_name: req.body.last_name,
								avatar: req.body.avatar
							};

							res.status(200).send({
								id_token: createToken(newUser)
							});
						}
					});
				}
			});
		});
	}
});

//login
router.post('/login',function(req,res,next){
	if (!req.body.email || !req.body.password) 
	{
		return res.status(400).send("Mohon masukkan email dan password!");
	} 
	else 
	{
		db.query("select * from user where email=?",[req.body.email],function(err,rows){
			if(rows.length > 0)
			{
				var passwordIsValid = bcrypt.compareSync(req.body.password, rows[0].password);
				if(!passwordIsValid)
				{
					res.status(400).send("Password yang anda masukkan salah!");
				}
				else
				{
					User = {
						id: rows[0].id_user,
						email: rows[0].email,
						//password: rows[0].password,
						first_name: rows[0].first_name,
						last_name: rows[0].last_name,
						avatar: rows[0].avatar
					};

					res.status(200).send({
						id_token: createToken(User)
					});
				}
			}
			else
			{
				res.status(400).send("Alamat email tidak terdaftar!");
			}
		});
	}
});

module.exports = router;