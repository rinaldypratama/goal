var express = require('express');
var router = express.Router();
var mapel = require('../model/mapel_model');
var auth = require('../auth.js');

//get All mata pelajaran list
router.get('/all/', auth.verifyToken, function(req,res,next){
	mapel.getAllMapel(function(err,rows){
		if(err)
		{
			res.json(err)
		}
		else
		{
			res.json(rows)
		}
	})
});

//get All mata pelajaran list by bidang studi
router.get('/:bidang?', auth.verifyToken, function(req,res,next){
	mapel.getMapelbyBidang(req.params.bidang, function(err,rows){
		if(err)
		{
			res.json(err)
		}
		else
		{
			res.json(rows)
		}
	})
});

//get mata pelajaran by Id
router.get('/id/:id?', auth.verifyToken, function(req,res,next){
	mapel.getMapelbyId(req.params.id, function(err,rows){
		if(err)
		{
			res.json(err)
		}
		else
		{
			res.json(rows)
		}
	})
});

//insert mata pelajaran
router.post('/', auth.verifyToken, function(req,res,next){
	console.log(req.body);
	mapel.addMapel(req.body,function(err,count){
		if(err)
		{
			res.json(err);
		}
		else{
			res.json(req.body);
		}
	})
});

//update mata pelajaran by ID, need variable id mata pelajaran
router.put('/:id', auth.verifyToken, function(req,res,next){
	mapel.updateMapel(req.params.id,req.body,function(err,rows){
		if(err)
		{
			res.json(err);
		}
		else
		{
			res.json(rows);
		}
	})
});

//delete mata pelajaran by ID, need variable id mata pelajaran
router.delete('/:id', auth.verifyToken, function(req,res,next){
	mapel.deleteMapel(req.params.id,function(err,count){
		if(err)
		{
			res.json(err);
		}
		else
		{
			res.json(count);
		}
	})
});

module.exports = router;