var db = require('../dbconnection');

var soal = {
	getAllSoal:function(id_mapel, callback){
		return db.query("select s.*, u.first_name, u.avatar from soal s join user u on s.id_user = u.id_user where id_mapel=? group by id_soal",
		[id_mapel],callback);
	},

	getSoalbyId:function(params, callback){
		return db.query("select s.*, u.first_name, u.avatar from soal s join user u on s.id_user = u.id_user where id_mapel=? AND id_soal=?",
		[params.mapel,params.soal],callback);
	},

	getSoalbyUser:function(params,callback){
		return db.query("select s.*, u.first_name, u.avatar from soal s join user u on s.id_user = u.id_user where s.id_user=? AND s.id_mapel=? group by id_soal",
		[params.user,params.mapel],callback);
	},

	addSoal:function(body, callback){
		db.getConnection(function(err, connection){
			connection.beginTransaction(function(err){
				if(err) { 
					connection.rollback(function(){
						connection.release();
						return callback(err);
					});
				}
				else {
					connection.query("insert into soal(id_mapel,id_user,soal,gambar,jawaban_benar,jawaban_1,jawaban_2,jawaban_3,jawaban_4,jawaban_5) values(?,?,?,?,?,?,?,?,?,?)",
					[body.id_mapel,body.id_user,body.soal,body.gambar,body.jawaban_benar,body.jawaban_1,body.jawaban_2,body.jawaban_3,body.jawaban_4,body.jawaban_5],
					function(err,results)
					{
					 	if(err)
					 	{
					 		connection.rollback(function(){
					 			connection.release();
					 			return callback(err);
					 		});
					 	}
					 	else
					 	{
					 		connection.commit(function(err){
					 			if(err)
					 			{
					 				connection.rollback(function(){
					 					connection.release();
					 					return callback(err);
					 				});
					 			}
					 			else
					 			{
					 				return callback(results);
					 			}
					 		});
					 	}
					});
				}
			});
		});
	},

	updateSoal:function(id, body, callback){
		db.getConnection(function(err, connection){
			connection.beginTransaction(function(err){
				if(err) { 
					return err;
					connection.rollback(function(){
						connection.release();
					});
				}
				else {
					connection.query("update soal set id_mapel=?, id_user=?, soal=?, gambar=?, jawaban_benar=?, jawaban_1=?, jawaban_2=?, jawaban_3=?, jawaban_4=?, jawaban_5=? where id_soal=?",
					[body.id_mapel,body.id_user,body.soal,body.gambar,body.jawaban_benar,body.jawaban_1,body.jawaban_2,body.jawaban_3,body.jawaban_4,body.jawaban_5,id],
					function(err,results)
					{
					 	if(err)
					 	{
					 		connection.rollback(function(){
					 			connection.release();
					 			return callback(err);
					 		});
					 	}
					 	else
					 	{
					 		connection.commit(function(err){
					 			if(err)
					 			{
					 				connection.rollback(function(){
					 					connection.release();
					 					return callback(err);
					 				});
					 			}
					 			else
					 			{
					 				return callback(results);
					 			}
					 		});
					 	}
					});
				}
			});
		});
	},

	deleteSoal:function(id, callback){
		db.getConnection(function(err, connection){
			connection.beginTransaction(function(err){
				if(err) { 
					connection.rollback(function(){
						connection.release();
						return callback(err);
					});
				}
				else {
					connection.query("delete from soal where id_soal=?",[id],function(err,results)
					{
						if(err) {
							connection.rollback(function(){
								connection.release();
					 			return callback(err);
					 		});
						}
						else {
							connection.commit(function(err){
								if(err)
								{
									connection.rollback(function(){
										connection.release();
										return callback(err);
									});
								}
								else
								{
									return callback(results);
								}
							});
						}
					});
				}
			});
		});
	},
};

module.exports = soal;