var db = require('../dbconnection');

var bidang_studi = {
	getAllBidang:function(callback){
		return db.query("select * from bidangstudi",callback);
	},

	getBidangbyId:function(id, callback){
		return db.query("select * from bidangstudi where id_bidang=?",[id],callback);
	},
};

module.exports = bidang_studi;