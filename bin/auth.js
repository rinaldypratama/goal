var jwt = require('jsonwebtoken');

module.exports = {
	'secret': 'gridGS',

	verifyToken: function(req, res, next){
		var token = req.headers['authorization'];

		if(!token)
		{
			return res.status(403).send({ auth: false, message: 'Gagal mengakses data' });
		}
		else
		{
			jwt.verify(token, module.exports.secret, function(err, decoded) {
				if(err)
				{
					return res.status(500).send({ auth: false, message: 'Otentikasi token gagal' });
				}
				else
				{
					req.userId = decoded.id;
					req.userEmail = decoded.email;
					req.userFName = decoded.first_name;
					req.userLName = decoded.last_name;
					req.userAvatar = decoded.avatar;
					next();
				}
			});
		}
	}
};