import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';
import { setCurrentUser, logoutUser } from './actions/userActions';
import './App.css';

import store from './store';

import PrivateRoute from './utils/PrivateRoute';
import Dashboard from './pages/Dashboard';
import LoginPage from './pages/LoginPage';
import StudyPage from './pages/StudyPage';

// Check for token
if (localStorage.jwtToken) {
  // Set auth token header auth
  setAuthToken(localStorage.jwtToken);
  // Decode token and get user info and exp
  const decoded = jwt_decode(localStorage.jwtToken);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  // Check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());
    // // Clear current Profile
    // store.dispatch(clearCurrentProfile());
    // Redirect to login
    window.location.href = '/';
  }
}

class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
        <Route exact path="/" component={LoginPage}/>
	      <Route path="/signup" component={LoginPage}/>
        <Switch>
          <PrivateRoute path="/studypage" component={StudyPage}/>
        </Switch>
        <Switch>
          <PrivateRoute path="/dashboard" component={Dashboard}/>
        </Switch>
      </div>
      </Router>
    );
  }
}

export default App;
