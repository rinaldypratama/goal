import { GET_STUDI, GET_MAPEL, SET_CHOICE, GET_ERRORS, STUDI_LOADING } from '../actions/types';


const initialState = {
	studi: [],
	mapel: [],
	choice: {
		mapel: '',
		id_mapel: ''
	},
	loading: false
};

export default function(state = initialState, action){
	switch(action.type){
		case STUDI_LOADING:
			return {
				...state,
        		loading: true
			};
		case GET_STUDI:
	      return {
	        ...state,
	        studi: action.payload
	      };
	   case GET_MAPEL:
	      return {
	        ...state,
	        mapel: action.payload,
	        loading: false
	      };
	   case SET_CHOICE:
	   	return{
	   		...state,
	   		choice: action.payload
	   	};
		default:
			return state;
	}
}