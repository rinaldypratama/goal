import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import soalReducer from './soalReducer';
import userReducer from './userReducer';
import errorReducer from './errorReducer';
import commonReducer from './commonReducer';

const rootReducer = combineReducers({
	user: userReducer,
	soal: soalReducer,
	error: errorReducer,
	common: commonReducer
});

const initialState = {};

const middleware = [thunk];
// eslint-disable-next-line
const composeMiddleware = compose(
	applyMiddleware(...middleware)
	// window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const store = createStore(
	rootReducer,
	initialState,
	// applyMiddleware(...middleware)
	composeMiddleware
);

export default store;