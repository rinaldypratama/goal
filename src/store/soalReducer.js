import {  GET_SOAL, SOAL_LOADING, SET_SOAL, EMPTY_SOAL  } from '../actions/types';

const initialState = {
	soal:{},
	soals: [],
	soalId: '',
	loading: false
};

export default function(state = initialState, action){
	switch(action.type){
		case SOAL_LOADING:
			return {
				...state,
        		loading: true
			};
		case GET_SOAL:
	      return {
	        ...state,
	        soals: action.payload,
	        loading:false
	      };
	      case SET_SOAL:
	      return {
	        ...state,
	        soal: action.payload,
	      };
	      case EMPTY_SOAL:
	      return {
	        ...state,
	        soal: {},
	      };
		default:
			return state;
	}
}