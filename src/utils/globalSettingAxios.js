import axios from 'axios';

export const globalURL = axios.defaults.baseURL = 'http://localhost:3030';
// export const globalURL = axios.defaults.baseURL = 'http://192.168.1.6:3030';
export const headerPostContent = axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
