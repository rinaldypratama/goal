import React from 'react';
import spinner from '../assets/spinner.gif';

export default () => {
  return (
    <div>
      <img
        src={spinner}
        style={{ width: '5vw', margin: 'auto', marginTop: '2em', display: 'block' }}
        alt="Loading..."
      />
    </div>
  );
};
