import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import qs from 'qs';
import jwt_decode from 'jwt-decode';

import { GET_ERRORS, SET_CURRENT_USER } from './types';

// Register User
export const registerUser = (userData) => dispatch => {
  axios
    .post('/user/signup/', userData)
    .then(res => {
      // Save to localStorage
      const { id_token } = res.data;
      // Set token to ls
      localStorage.setItem('jwtToken', id_token);
      // Set token to Auth header
      setAuthToken(id_token);
      // Decode token to get user data
      const decoded = jwt_decode(id_token);
      // Set current user
      dispatch(setCurrentUser(decoded));
      // console.log('GoodResponse', decoded);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Login - Get User Token
export const loginUser = userData => dispatch => {
  axios
    .post('user/login/', userData)
    .then(res => {
      // Save to localStorage
      const { id_token } = res.data;
      // Set token to ls
      localStorage.setItem('jwtToken', id_token);
      // Set token to Auth header
      setAuthToken(id_token);
      // Decode token to get user data
      const decoded = jwt_decode(id_token);
      // Set current user
      dispatch(setCurrentUser(decoded));
      // console.log('GoodResponse', res);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const updateUser = (idUser, userData, history) => dispatch => {
  axios
    .put(`user/${idUser}`, userData)
    .then(res => history.push('/dashboard/daftar-soal'))
      .catch(err =>
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        })
      );
}

// Set logged in user
export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

// Log user out
export const logoutUser = () => dispatch => {
  // Remove token from localStorage
  localStorage.removeItem('jwtToken');
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to {} which will set isAuthenticated to false
  dispatch(setCurrentUser({}));
};
