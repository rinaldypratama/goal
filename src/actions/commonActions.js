import axios from 'axios';

import { GET_STUDI, GET_MAPEL, SET_CHOICE, GET_ERRORS, STUDI_LOADING } from './types';

export const getStudi = () => dispatch => {
   dispatch(setStudiLoading());
		axios
			.get('/studi')
			.then(res =>
				dispatch({
					type: GET_STUDI,
					payload: res.data
				})
			)
			.catch(err =>
				dispatch({
					type: GET_STUDI,
					payload: null
				})
			);
	dispatch(getMapel());
};

export const getMapel = () => dispatch => { 
		axios
			.get('/mapel/all')
			.then(res =>
				dispatch({
					type: GET_MAPEL,
					payload: res.data
				})
			)
			.catch(err =>
				dispatch({
					type: GET_MAPEL,
					payload: null
				})
			);
};

export const setChoice = choice => {
	return {
		type: SET_CHOICE,
		payload: choice
	}
};

export const setStudiLoading = () => {
  return {
    type: STUDI_LOADING
  };
};