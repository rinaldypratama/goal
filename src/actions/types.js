export const GET_ERRORS = 'GET_ERRORS';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const GET_STUDI = 'GET_STUDI';
export const GET_MAPEL = 'GET_MAPEL';
export const SET_CHOICE = 'GET_CHOICE';
export const STUDI_LOADING = 'STUDI_LOADING';
export const ADD_SOAL = 'ADD_SOAL';
export const GET_SOAL = 'GET_SOAL';
export const SET_SOAL = 'SET_SOAL';
export const EMPTY_SOAL = 'EMPTY_SOAL';
export const SOAL_LOADING = 'SOAL_LOADING';


