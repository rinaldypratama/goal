import axios from 'axios';
import qs from 'qs';

import { GET_ERRORS, GET_SOAL, GET_DETAIL_SOAL, UPDATE_SOAL, SOAL_LOADING, SET_SOAL, EMPTY_SOAL } from './types';

// Get Soal from api
export const getSoal = (idMapel) => dispatch => {
	dispatch(setSoalLoading());
		axios
			.get(`/soal/mapel/${idMapel}`)
			.then(res =>
				dispatch({
					type: GET_SOAL,
					payload: res.data
				})
			)
			.catch(err =>
				dispatch({
					type: GET_ERRORS,
					payload: err.response.data
				})
			);
};

//Update Soal
export const updateSoal = (idSoal, soalData, history) => dispatch => {
  axios
    .put(`/soal/${idSoal}`,soalData)
    .then(res => history.push('/dashboard/daftar-soal'))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const deleteSoal = (idSoal, idUser, idMapel, history) => dispatch => {
  axios
    .delete(`/soal/${idSoal}`)
    .then(res => {
		dispatch(getSoal(idUser, idMapel));
    	history.push('/dashboard/daftar-soal');
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Add Soal
export const addSoal = (soalData, history) => dispatch => {
  axios
    .post('/soal',soalData)
    .then(res => history.push('/dashboard/daftar-soal'))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const setSoal = (soalData, history) => dispatch =>{
  dispatch({
    type:SET_SOAL,
    payload:soalData
  });
}

export const emptySoal = () =>{
  return{
    type:EMPTY_SOAL
  }
}


export const setSoalLoading = () => {
  return {
    type: SOAL_LOADING
  };
};
