import React, { Component } from 'react';
import './ModalImage.css';

class ModalImage extends Component {
	componentDidMount(){
		document.body.style.overflow = "hidden";
	}
	componentWillUnmount(){
		document.body.style.overflow = "auto";
	}

	onClick(){
		this.props.modalHandler()
	}

	closeHandle(e){
		if(e.target.className === 'modal-overlay'){
			this.props.modalHandler();
		}
	}

	render() {
		const { img } = this.props;
		return (
		 <div className='modal-overlay' onClick={(e)=>this.closeHandle(e)}>
		  <div className='image-container'>
		    <img src={img} alt="detail-image"/>
		  </div>
			{/*Style follow close-btn from PrintSoal*/}
		  <button className="close-btn" onClick={this.onClick.bind(this)}>X</button>
		 </div>
		);
	}
}


export default ModalImage;