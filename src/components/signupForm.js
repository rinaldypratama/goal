import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { registerUser } from '../actions/userActions';

class signupForm extends Component{
	constructor(props){
		super(props);
		this.state = {
			first_name: '',
			last_name: '',
			email: '',
			password: '',
			error: ''
		};
		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	onSubmit(e) {
   	e.preventDefault();

    	const userData = {
	      first_name: this.state.first_name,
	      last_name: this.state.last_name,
	      email: this.state.email,
	      password: this.state.password
    	};
	   this.props.registerUser(userData);
  	}
	onChange(e) {
   	this.setState({ [e.target.name]: e.target.value });
  	}

	render() {
		return (
			<div>
				<h2>DAFTAR</h2>
				<form className="form" onSubmit={this.onSubmit} >
					<input type="text" 
							 placeholder='Nama Depan' 
							 name='first_name'
							 value={this.state.first_name} 
							 onChange={this.onChange}
							 />
					<br/>
					<input type="text" 
							 placeholder='Nama Belakang' 
							 name='last_name'
							 value={this.state.last_name} 
							 onChange={this.onChange}
							 />
					<br/>
					<input type="Email" 
							 placeholder='Email' 
							 name='email'
							 value={this.state.email}
							 onChange={this.onChange}
							 />
					<br/>
					<input type="Password" 
							 placeholder='Password' 
							 name='password'
							 value={this.state.password}
							 onChange={this.onChange}
							 />
					<br/>	
					<Link to="/">sudah punya akun?</Link>
					<button type="submit" className="btn btn-info btn-sm" value='Submit'>DAFTAR</button>
				</form>
			</div>
		);
	}
}

const mapStateToProps = state => ({
  user: state.user,
  error: state.error
});

export default connect(mapStateToProps, {registerUser}) (signupForm);