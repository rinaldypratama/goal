import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addSoal, updateSoal, emptySoal } from '../actions/soalActions';
import { getError } from '../actions/errorActions';
import qs from 'qs';
import './TambahSoal.css'

import ModalInfo from './ModalInfo';
import ModalError from './ModalError';
import uploadLogo from '../assets/upload.png';


class TambahSoal extends Component {
	constructor(props) {
    super(props);
    this.state = {
    	idSoal:'',
    	soal:'',
    	jawaban1:'',
    	jawaban2:'',
    	jawaban3:'',
    	jawaban4:'',
    	jawaban5:'',
    	jawaban_benar:'',
    	gambar:'',
    	modal:false,
    	edit:false
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onClickModalHandler = this.onClickModalHandler.bind(this);
  }

  	componentDidMount(){
  		window.scrollTo(0,0);
  		// console.log(Object.getOwnPropertyNames(this.props.soal.soal).length);
  		if (Object.getOwnPropertyNames(this.props.soal.soal).length !== 0) {
  			console.log('cek props.soal');
 			const { id_soal, soal, gambar, jawaban_benar, jawaban_1, jawaban_2, jawaban_3, jawaban_4, jawaban_5 } = this.props.soal.soal;
	    	this.setState({ 
	    		edit:true,
	    		idSoal: id_soal,
		      soal,
		      gambar,
		      jawaban1:jawaban_1,
		      jawaban2:jawaban_2,
		      jawaban3:jawaban_3,
		      jawaban4:jawaban_4,
		      jawaban5:jawaban_5
	  	  })
	   }
  	}

  	componentWillUnmount(){
  		this.props.emptySoal();
  	}

  	onSubmit(e) {
   	e.preventDefault();

   	const { id_mapel } = this.props.common.choice;
   	const { id } = this.props.user.user;

    	const soalData = {
	      id_mapel,
	      id_user: id,
	      soal: this.state.soal,
	      gambar: this.state.gambar,
	      jawaban_benar: this.state.jawaban_benar,
	      jawaban_1: this.state.jawaban1,
	      jawaban_2: this.state.jawaban2,
	      jawaban_3: this.state.jawaban3,
	      jawaban_4: this.state.jawaban4,
	      jawaban_5: this.state.jawaban5
    	};
    	if(!soalData.soal){
    		this.props.getError('Soal tidak boleh kosong')
    	}
    	else if(!soalData.jawaban_1 || !soalData.jawaban_2 || !soalData.jawaban_3 || !soalData.jawaban_4 || !soalData.jawaban_5){
    		this.props.getError('Pilihan jawaban harus terisi semua')
    	}
    	else if(!soalData.jawaban_benar){
    		this.props.getError('Pilih jawaban yang benar')
    	}
    	else if(!this.state.edit){
    		console.log('masuk tambah');
	    	this.props.addSoal(soalData, this.props.history);
    	}
    	else{
    		console.log('masuk edit');
    		this.props.updateSoal(this.state.idSoal, soalData, this.props.history);
    	}
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
    // console.log(this.state);
  }

  onClickModalHandler(){
		this.setState({
			modal:!this.state.modal
		});
	}

	render() {
		const {error} = this.props;
		return (
			<div className='ts-wrap'>
				<form className='ts-wrapper'>
					<div className="ts-first">
						<h3>Soal<span>*)</span></h3>
						<textarea 
							name="soal" 
							value={this.state.soal} 
							onChange={this.onChange}
							placeholder='Tulis soal disini.....'/>
						<p>*) WAJIB DIISI</p>
					</div>
					<div className="ts-second">
						<h3>Jawaban<span>*)</span></h3>
						<br/>
						<div className="form-check">
					        <input className="formRadio" type="radio" name="jawaban_benar" onChange={this.onChange} id="answerRadios1" value={this.state.jawaban1}/>
					        <label className="form-check-label" htnlfor="answerRadios1">A</label>
					        <input className='formInput' type="text" name='jawaban1' onChange={this.onChange} value={this.state.jawaban1}/>
					        <br/>
					        <input className="formRadio" type="radio" name="jawaban_benar" onChange={this.onChange} id="answerRadios2" value={this.state.jawaban2}/>
					        <label className="form-check-label" htnlfor="answerRadios2">B</label>
					        <input className='formInput' type="text" name='jawaban2' onChange={this.onChange} value={this.state.jawaban2}/>
					        <br/>
					        <input className="formRadio" type="radio" name="jawaban_benar" onChange={this.onChange} id="answerRadios3" value={this.state.jawaban3}/>
					        <label className="form-check-label" htnlfor="answerRadios3">C</label>
					        <input className='formInput' type="text" name='jawaban3' onChange={this.onChange} value={this.state.jawaban3}/>
					        <br/>
					        <input className="formRadio" type="radio" name="jawaban_benar" onChange={this.onChange} id="answerRadios4" value={this.state.jawaban4}/>
					        <label className="form-check-label" htnlfor="answerRadios4">D</label>
					        <input className='formInput' type="text" name='jawaban4' onChange={this.onChange} value={this.state.jawaban4}/>
					        <br/>
					        <input className="formRadio" type="radio" name="jawaban_benar" onChange={this.onChange} id="answerRadios5" value={this.state.jawaban5}/>
					        <label className="form-check-label" htnlfor="answerRadios5">E</label>
					        <input className='formInput' type="text" name='jawaban5' onChange={this.onChange} value={this.state.jawaban5}/>
				        </div>
				        <p>*) WAJIB DIISI</p>
						<p>*) PILIH JAWABAN YANG BENAR DENGAN MEMILIH LINGKARAN DI SEBELAH HURUF</p>
					</div>
					<div className="ts-third">
						<h3>Unggah Gambar</h3>
						<div className="uploadBox">
						<img src={uploadLogo} alt='upload' width="50" height="50"/>
						<input type="file" id="input"/>
						</div>
						<p>*) JIKA SOAL MEMERLUKAN ILUSTRASI/GAMBAR, SILAHKAN UNGGAH GAMBAR PADA BOX DIATAS</p>
						<input className='btn btn-outline-secondary submitBtn' type="button" value="Submit" onClick={this.onClickModalHandler}/>
					</div>
				</form>
				{
					this.state.modal &&
					<ModalInfo title='tambah' modalHandler={this.onClickModalHandler} confirmHandler={this.onSubmit}/>
				}
				{
						error &&
						<ModalError error={error} title='Cek Soal'/>
				}	
			</div>
		);
	}
}

const mapStateToProps = state => ({
  common: state.common,
  user: state.user,
  error: state.error,
  soal: state.soal
});

export default connect(mapStateToProps, {addSoal, getError, updateSoal, emptySoal}) (TambahSoal);