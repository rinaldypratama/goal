import React, { Component } from 'react';
import { connect } from 'react-redux';
import ModalImage from './ModalImage';
import './ModalDetail.css';

import InfoLogo from '../assets/info.png';
import AuthorLogo from '../assets/author.png';
import JawabanLogo from '../assets/jawaban.png';
import DefAva from '../assets/blankProfilePicture.png';

class ModalDetail extends Component {
	constructor(props){
		super(props);
		this.state = {
			modalImg:false
		};

	}
	
	componentDidMount(){
		document.body.style.overflow = "hidden";
	}
	componentWillUnmount(){
		document.body.style.overflow = "auto";
	}

	onClickModalHandler(){
		this.setState({
			modalImg:!this.state.modalImg
		});
  }

	getAlphabet(){
		const { jawaban_benar: benar, 
				  jawaban_1: j1, 
				  jawaban_2: j2, 
				  jawaban_3: j3, 
				  jawaban_4: j4, 
				  jawaban_5: j5 } = this.props.detailSoal;

		return benar==j1?
									'A': benar==j2?
									'B': benar==j3?
									'C': benar==j4?
									'D': benar==j5?
									'E':'---';


	}

	closeHandle(e){
		if(e.target.className === 'modal-overlay'){
		// console.log('e',e.target.className)
			this.props.modalHandler();
		}
	}

	render() {
		const {	soal, modalHandler, detailSoal } = this.props;
		// const { first_name, avatar} = this.props.user.user;
		// console.log('modal', typeof(detailSoal.jawaban));

	  return (
	    <div className='modal-overlay' onClick={(e)=>this.closeHandle(e)}>
        <div className='modal-container'>
          <div className="modal-header">
          	<h5 className='txt-bold'>Detail Soal</h5>
          </div>
          <div className="modal-content">
	          <div className="left-content">
	          	<div className="content-soal">
	          		<p className='txt-bold'>SOAL</p>
	          		<p className='txt-small'>{detailSoal.soal}</p>
	          	</div>
	          	<div className="content-jawaban">
	          		<p className='txt-bold'>JAWABAN</p>
	          		<ol className='txt-small' type='A'>
	          			<li>{detailSoal.jawaban_1}</li>
	          			<li>{detailSoal.jawaban_2}</li>
	          			<li>{detailSoal.jawaban_3}</li>
	          			<li>{detailSoal.jawaban_4}</li>
	          			<li>{detailSoal.jawaban_5}</li>
	          		</ol>
							</div>
	          </div>
	          <div className="right-content">
	          	<div className="content-info-title">
		          	<img src={InfoLogo} alt="Info" className="right-content-logo"/>
		          	Info Soal
	          	</div>
					<div className="content-info">
					<img src={AuthorLogo} alt="author" className="right-content-logo"/>
					<span >Pembuat Soal</span> 
					<p className='push-content txt-bold'>
					<img src={DefAva || detailSoal.avatar} alt="avatar" className='right-content-logo round-ava' width="40" height='40'/>
					{detailSoal.first_name.charAt(0).toUpperCase() + detailSoal.first_name.slice(1).toLowerCase()}
					</p>
					<img src={JawabanLogo} alt="jawaban" className="right-content-logo"/>
					<span>Jawaban Benar</span>
					<p className='push-content txt-bold'>{this.getAlphabet()}</p>
					{
						(detailSoal.gambar == "" || detailSoal.gambar == null ) ?
						null:
						<React.Fragment>
							<span>Gambar</span>
							<img src={detailSoal.gambar} alt="Gambar Soal" className="soal-pict" onClick={this.onClickModalHandler.bind(this)}/>
						</React.Fragment>
					}
					</div>	
	          </div>
          </div>
          <div className="modal-footer">
          <button className='btn btn-secondary btn-footer' onClick={modalHandler}>Tutup</button>
          </div>
        </div>
			{
				this.state.modalImg && <ModalImage img={detailSoal.gambar} modalHandler={this.onClickModalHandler.bind(this)}/>
			}
	    </div>
	  );;
	}
}

const mapStateToProps = state => ({
  	user:state.user,
  	soal:state.soal
});

export default connect (mapStateToProps, null)(ModalDetail);