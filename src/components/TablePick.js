import React, { Component } from 'react';
import PrintSoal from './PrintSoal';

class TablePick extends Component {
	constructor(props) {
    super(props);
	    this.state = {
	    	soal: [],
	    	ref: new Set(),
	    	checkAll:false,
	    	modal:false
	    };

	   this.onChange = this.onChange.bind(this);
	   this.onSubmit = this.onSubmit.bind(this);
	   this.modalHandler =this.modalHandler.bind(this);
	   this.checkAllHandler = this.checkAllHandler.bind(this);
	   this.checkHandler= this.checkHandler.bind(this);
 	}

 	componentDidMount() {
	}

	modalHandler(){
		this.setState({
			modal:!this.state.modal
		})
	}

	checkAllHandler(){
		const { ref, checkAll } = this.state;
		const { data } = this.props;
		let change = checkAll;

		if(!checkAll){
			data.map(x=>{
				if(!ref.has(x.id_soal)){
					ref.add(parseInt(x.id_soal));
					// console.log(x.id_soal);
				}
			});
			change = true;
			// console.log('tidak checkall', ref);
		}
		else if(checkAll){
			ref.clear();
			change = false;
			// console.log('checkall');
		}

		this.setState({
			ref,
			checkAll : change
		});
	}

	checkHandler(id){
		const { ref } = this.state;
		// console.log(ref.has(id));
		return ref.has(id);
	}

	onChange(e) {
 		const value = parseInt(e.target.value);
 		const { ref, checkall } = this.state;
 		let checkAll = this.state.checkAll;

 		if(ref.has(value)){
 			// console.log('delete value');
 			ref.delete(value);
 			checkAll = false;
 		}
 		else{
 			ref.add(value);
 			// console.log('tambah value', ref);
 		}
		this.setState({
			ref,
			checkAll
		});
 	}

 	onSubmit(){
 		const { data } = this.props;
 		const { ref } = this.state;
 		const printSoal = data.filter(x=> ref.has(x.id_soal));
 		this.setState({
 			soal: printSoal,
 			modal:true
 		})
 	}

	render() {
		const { data } = this.props;
		const { modal, soal, checkAll } = this.state;
		return (
			<div>
				<button className="btn btn-submit block-div" onClick={this.onSubmit}>Print Soal</button>
				<div className="checkAll">
					<input type="checkbox" name="checkAll" checked={checkAll} onChange={this.checkAllHandler} value='checkAll'/>
					<p>Pilih Semua Soal</p>
				</div>
				<p>Anda Memilih : {this.state.ref.size} Soal </p>
				<table className="cust-table table table-striped center-table">
		      <thead>
		        <tr>
		          <th>Pilih</th>
		          <th>ID Soal</th>
		          <th>Soal</th>
		        </tr>
		      </thead>
		      <tbody>
		        {
		        	data.length == 0 ?
		        	<tr >
		          	<td colSpan="3" style={{textAlign:'center'}}>
		          		Belum ada soal
		          	</td>
		          </tr>:
		          data.map(row => (
		            <tr key={row.id_soal}>
		              <td>
		              	<input type="checkbox" name="soal" checked={this.checkHandler(row.id_soal)} onChange={this.onChange} value={row.id_soal}/>
		              </td>
		              <td>{row.id_soal}</td>
		              <td className="soal-align">{row.soal}</td>
		            </tr>
		          ))
		        }
		      </tbody>
		    </table>
			{
				(modal && soal.length != 0) &&
				<PrintSoal modalHandler={this.modalHandler} data={this.state.soal}/>
			}
			</div>
		);
	}
}

export default TablePick;