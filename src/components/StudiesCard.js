import React, { Component } from 'react';
import './StudiesCard.css';
import defLogo from '../assets/logo-buku.png';

class StudiesCard extends Component {
	onClick(mapel,id_mapel){
		const choice = {
			mapel:mapel,
			id_mapel
		}
		this.props.setChoice(choice);
		this.props.history.push('/dashboard/daftar-soal');
		// console.log('history', typeof(id_mapel));
	}

	render() {
		const { id_mapel } = this.props.studies;
		const mapel = this.props.studies.mapel || 'Unknown Studies';
		// const check = mapel === 'Biologi' || mapel === "Ekonomi"|| mapel === "Fisika"|| mapel === "Geografi" || mapel === "Kesenian" || mapel === "Kewarganegaraan" || mapel === "Kimia" || mapel === "Matematika" || mapel === "Olahraga" || mapel === "Sejarah" || mapel === "Sosial";
		const check = mapel === 'Biologi' || mapel === "Ekonomi"|| mapel === "Fisika"|| mapel === "Geografi" || mapel === "Kesenian" || mapel === "Kewarganegaraan" || mapel === "Kimia" || mapel === "Matematika" || mapel === "Olahraga" || mapel === "Sejarah" || mapel === "Sosial";
		const logo = check ? require(`../assets/logo-${mapel.toLowerCase()}.png`) : defLogo;
		return (
			<div 
				className='cardWrapper'
				onClick={this.onClick.bind(this, mapel, id_mapel)}>
				{/*<Link to="/dashboard">*/}
					<div className="cardIcon">
					<img src={logo} alt={mapel} width="130" height="130"/>
					</div>
					<div className="cardName">
						{mapel}
					</div>
				{/*</Link>*/}
			</div>
		);
	}
}

export default StudiesCard;