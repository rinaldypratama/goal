import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logoutUser } from '../actions/userActions';
import './Header.css';

import logoutIcon from '../assets/logout.png'

class Header extends Component {


	render() {
		return (
			<div className="header">
				<div className="logoutBtn" onClick={this.props.logoutUser}>
						<img src={logoutIcon} alt="Logout Icon"/>Keluar
				</div>
			</div>
		);
	}
}

export default connect (null, {logoutUser})(Header);