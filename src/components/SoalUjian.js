import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getSoal } from '../actions/soalActions';
import './SoalUjian.css';

import TablePick from './TablePick';
import Spinner from '../utils/Spinner';

class SoalUjian extends Component {
	constructor(props){
		super(props);
		this.state = {
    	};
	}

	componentDidMount() {
		window.scrollTo(0,0);
		const { id_mapel } = this.props.common.choice;
    	this.props.getSoal(id_mapel);
  	}

  	componentWillReceiveProps(nextProps) {
 		if (nextProps.soal) {
	    	this.setState({ 
    			soals: nextProps.soal.soals,
	        	renderSoal: nextProps.soal.soals.slice(0, this.state.view),
	        	totalSoal: nextProps.soal.soals.length
	  	  })
	   }
  	}

  	topButton (){
  		window.scrollTo({
		    top: 0,
		    behavior: "smooth"
		});
  	}

	render() {
		const { soals, loading } = this.props.soal;
		const { renderSoal, view, page, totalSoal } = this.state;
		return (
			<div className="su-wrapper">
				<div className="su-top">
				<h4>Pilih soal yang akan digunakan</h4>
				<br/>	
				{
					<input type='button' className="to-top-btn" onClick={this.topButton} value='&uArr;'/>
				}
				</div>
				{
						loading?
						<Spinner/>:
						<TablePick data={soals}/>
				}	
			</div>
		);
	}
}

const mapStateToProps = state => ({
  	common:state.common,
  	soal:state.soal,
  	user:state.user
});

export default connect(mapStateToProps, { getSoal })(SoalUjian);