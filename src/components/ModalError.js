import React, { Component } from 'react';
import { connect } from 'react-redux';
import { clearErrors } from '../actions/errorActions';
import './ModalError.css';

import ErrorLogo from '../assets/error.png';


class ModalError extends Component {
	componentDidMount(){
		document.body.style.overflow = "hidden";
	}
	componentWillUnmount(){
		document.body.style.overflow = "auto";
	}
	onClick(){
		this.props.clearErrors()
	}

	closeHandle(e){
		if(e.target.className === 'modal-overlay'){
		// console.log('e',e.target.className)
			this.props.clearErrors();
		}
	}

	render() {
		const { title, error } = this.props;
		// console.log('modalerror', error);
	  return (
	    <div className='modal-overlay' onClick={(e)=>this.closeHandle(e)}>
        <div className='error-container'>
          <div className="header-error">
          	<img src={ErrorLogo} alt="error"/>
          </div>
          <div className="content-error">
				<h3><strong>{title.toUpperCase()}</strong></h3>
				<p>{error}</p>
          </div>
          <div className="footer-error">
          	<button className='btn btn-error' onClick={this.onClick.bind(this)}>Tutup</button>
          </div>
        </div>
	    </div>
	  );
	}
}


export default connect(null, {clearErrors})(ModalError);