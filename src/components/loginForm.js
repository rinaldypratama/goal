import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { loginUser } from '../actions/userActions';

class loginForm extends Component{
	constructor(props){
		super(props);
		this.state = {
	      email: '',
	      password: '',
	      errors: {}
	    };

   this.onSubmit = this.onSubmit.bind(this);
   this.onChange = this.onChange.bind(this);
	}

	onSubmit(e) {
   	e.preventDefault();

    	const userData = {
	      email: this.state.email,
	      password: this.state.password
    	};
    	
	   this.props.loginUser(userData);
  	}
	
	onChange(e) {
 		this.setState({ [e.target.name]: e.target.value });
 	}

	render() {

		return (
			<div>
				<h2>LOGIN</h2>
				<form className="form" onSubmit={this.onSubmit}>
					<input type="Email" placeholder='Email' name='email' onChange={this.onChange} value={this.state.email} />
					<br/>
					<input type="Password" placeholder='Password' name='password' onChange={this.onChange} value={this.state.password} />
					<br/>			
					<Link to="/signup">butuh akun?</Link>
						<button type="submit" className="btn btn-info btn-sm">LOGIN</button>
				</form>
					
			</div>
		);
	}
}

const mapStateToProps = state => ({
	user:state.user,
  	error:state.error
});

export default connect (mapStateToProps, {loginUser})(loginForm);