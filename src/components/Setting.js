import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getError } from '../actions/errorActions';
import { updateUser } from '../actions/userActions';
import './Setting.css'

import ModalError from './ModalError';


class Setting extends Component {
	constructor(props) {
    	super(props);
    	this.state = {
    		password: '',
    		repassword: ''
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

	componentDidMount(){
		window.scrollTo(0,0);
	}

	onSubmit(e) {
   	e.preventDefault();
   	console.log('onSUbmit');
   	if(this.state.password !== this.state.repassword){
   		this.props.getError('Password tidak sama, coba lagi!')
   		console.log('gak sama')
   	}
   	else{
   		const { id, first_name, last_name, email, avatar } = this.props.user.user;
   		const userData = {
	      first_name,
	      last_name,
	      email,
	      avatar,
	      password: this.state.password
    	};
    	this.props.updateUser(id, userData, this.props.history);
    	console.log('lanjut');
   	}
 	}

 	onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

	render() {
		const { error } = this.props;
		return (
			<div className='setting-wrapper'>
				<div className="header-setting">
					<h4>Ganti Password</h4>
				</div>
				<div className="content-setting">
				<form onSubmit={this.onSubmit}>
					<label htmlFor="password">Password</label>
					{/*<br/>*/}
					<input type="password" 
							 placeholder='Password' 
							 name='password'
							 value={this.state.password} 
							 onChange={this.onChange}
							 />
					<br/>
					<label htmlFor="repassword">Ulangi Password</label>
					{/*<br/>*/}
					<input type="password" 
							 placeholder='Password' 
							 name='repassword'
							 value={this.state.repassword} 
							 onChange={this.onChange}
							 />
					<br/>
					<button type='submit' className='btn btn-submit'>Ganti</button>
				</form>
				</div>
				{
					error &&
					<ModalError error={error} title='Cek Password'/>
				}
			</div>
		);
	}
}

const mapStateToProps = state => ({
  user: state.user,
  error: state.error
});

export default connect(mapStateToProps, {getError, updateUser}) (Setting);