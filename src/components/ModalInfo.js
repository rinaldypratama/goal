import React, { Component } from 'react';
import './ModalInfo.css';

import InfoLogo from '../assets/logo-konfirmasi.png';


class ModalInfo extends Component {
	constructor(props) {
    	super(props);

   	this.onClick = this.onClick.bind(this);
  	}

 	componentDidMount(){
		document.body.style.overflow = "hidden";
	}
	componentWillUnmount(){
		document.body.style.overflow = "auto";
	}
	onClick(e){
		const { title, modalHandler, confirmHandler, deleteHandler } = this.props;
		if(title == 'tambah'){
			modalHandler();
			confirmHandler(e);
		}	
		else{
			modalHandler();
			deleteHandler();
		}
	}	

	closeHandle(e){
		if(e.target.className === 'modal-overlay'){
		// console.log('e',e.target.className)
			this.props.modalHandler();
		}
	}

	render() {
		const { title, modalHandler, confirmHandler } = this.props;
		const txt = title === 'tambah'? 'Apakah anda akan menambahkan soal?' : 'Apakah anda ingin menghapus soal?';
		// console.log('modalerror', error);
	  return (
	    <div className='modal-overlay' onClick={(e)=>this.closeHandle(e)}>
        <div className='info-container'>
          <div className="header-info">
           <h3>Konfirmasi</h3>
          </div>
          <div className="content-modalInfo">
          	<img src={InfoLogo} alt="info"/>
				<h3><strong>{txt}</strong></h3>
          </div>
          <div className="footer-info">
				<p>Pilih Ya untuk lanjut, atau Tidak untuk kembali</p>
          	<button className='btn btn-info btn-modalInfo' onClick={(e)=>this.onClick(e)}>Ya</button>
          	<button className='btn btn-info btn-modalInfo btn-info-def' onClick={modalHandler}>Tidak</button>
          </div>
        </div>
	    </div>
	  );;
	}
}


export default ModalInfo;