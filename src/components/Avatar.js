import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import './Avatar.css';
import defAva from '../assets/blankProfilePicture.png';

class Avatar extends Component {
	constructor(props) {
	  super(props);
	  this.ucFirst = this.ucFirst.bind(this);
	}
	//make first character uppercase
	ucFirst(string){
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
	}

	render() {
		const { first_name, last_name, email, avatar} = this.props.user.user;
		const username = email.split('@');
		return (
			<div className='avatarWrapper'>
				<div className="avaImage">
				<img src={defAva || avatar} alt='avatar'/>
				</div>
				<NavLink to='/dashboard/setting' activeClassName="menuSelect">
					<div className="avaName">
					{this.ucFirst(first_name)} <br/> {this.ucFirst(last_name)}
					</div>	
					<div className="avaId">{username[0]}</div>
				</NavLink>
			</div>
		);
	}
}

const mapStateToProps = state => ({
  user:state.user
});


export default connect(mapStateToProps, null)(Avatar);