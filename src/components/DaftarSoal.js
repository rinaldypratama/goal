import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getSoal } from '../actions/soalActions';
import './DaftarSoal.css';

import Pagination from './Pagination';
import TableList from './TableList';
import Spinner from '../utils/Spinner';
import ModalDetail from './ModalDetail'

class DaftarSoal extends Component {
	constructor(props){
		super(props);
		this.state = {
      soals: [],
      renderSoal: [],
      totalSoal: null,
      id_mapel: '',
      view: 10,
      page: 1,
      showModal: false
    };

   this.onClickModalHandler = this.onClickModalHandler.bind(this);
   this.onClickSoalDetail = this.onClickSoalDetail.bind(this);
   this.onChange = this.onChange.bind(this);
   this.handlePageChange = this.handlePageChange.bind(this);
	}

	componentDidMount() {
    window.scrollTo(0,0);
		const { id_mapel } = this.props.common.choice;
    	this.props.getSoal(id_mapel);
  	}

	componentWillReceiveProps(nextProps) {
 		if (nextProps.soal) {
    	this.setState({ 
    		soals: nextProps.soal.soals,
        renderSoal: nextProps.soal.soals.slice(0, this.state.view),
        totalSoal: nextProps.soal.soals.length,
        page:1
  	  })
    }
  }

  handlePageChange(page) {
    const renderSoal = this.state.soals.slice((page - 1) * this.state.view, page * this.state.view);
    // in a real app you could query the specific page from a server user list
    this.setState({ page, renderSoal });
    window.scrollTo({
        top: 0,
        behavior: "smooth"
    });
  }

	onClickModalHandler(){
		this.setState({
			showModal:!this.state.showModal
		});
  }

  onClickSoalDetail(detailSoal){
    this.setState({
      soal:detailSoal,
      showModal: true
    });
	}

  onChange(e) {
    this.setState({ 
      [e.target.name]: e.target.value,
      page:1,
      renderSoal: this.state.soals.slice(0, e.target.value)
    });
  }

	render() {
		const { loading } = this.props.soal;
    const { renderSoal, view, page, totalSoal } = this.state;
    const { id } = this.props.user.user;
    const { id_mapel } = this.props.common.choice;
		return (
			<div className="ds-wrapper">
				<div className="ds-top">
          <Link to='/dashboard/tambah-soal'>
            <button className='btn btn-outline-secondary addButton'>Tambah Soal</button>
          </Link>
					<br/>	
					<select className="custom-select slcInput" onChange={this.onChange} name='view' id="inputGroupSelect01" value={this.state.view}>
						<option defaultValue value="10">10</option>
						<option value="25">25</option>
						<option value="50">50</option>
					</select>
					<p>Tampilan per halaman</p>	
				</div>
				{
						loading?
						<Spinner/>:
						<TableList data={renderSoal} clickSoal={this.onClickSoalDetail} history={this.props.history} throw={{id, id_mapel}}/>
        }   
        {
          this.state.showModal && 
          <ModalDetail detailSoal={this.state.soal} modalHandler={this.onClickModalHandler}/>
        }
            <Pagination
              margin={2}
              page={page}
              count={Math.ceil(totalSoal / view)}
              onPageChange={this.handlePageChange}
            />
			</div>
		);
	}
}

const mapStateToProps = state => ({
  	common:state.common,
  	soal:state.soal,
    user:state.user
});

export default connect(mapStateToProps, { getSoal }) (DaftarSoal);