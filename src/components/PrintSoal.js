import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactToPrint from "react-to-print";
import './PrintSoal.css';

class PrintSoal extends Component {
	componentDidMount(){
		document.body.style.overflow = "hidden";
	}
	componentWillUnmount(){
		document.body.style.overflow = "auto";
	}
	onClick(){
		this.props.modalHandler();
	}

	closeHandle(e){
		if(e.target.className === 'soal-overlay'){
			this.props.modalHandler();
		}
	}

	render() {
		const { common, data } = this.props;
	  return (
	    <div className='soal-overlay' onClick={(e)=>this.closeHandle(e)}>
    		<button className="close-btn" onClick={this.onClick.bind(this)}>X</button>
    		<ReactToPrint
           trigger={() => <a className='print-txt' href="#">Cetak Halaman Ini!</a>}
           content={() => this.componentRef}
         />
        	<div className='soal-container' ref={el => (this.componentRef = el)}>
	        	<div className="soal-header">
				<h1>{common.choice.mapel}</h1>
				<h5>Nama :</h5>
				<h5>Kelas :</h5>
				<h5>Absen :</h5>
	        	</div>
	         <div className="soal-content">
					{
						data.map((soal, index) => (
							<div className="soal-wrap" key={index}>
								<div className="no-soal">{(index+1)+'. '}</div>
								<div className="soal-p">
								{soal.soal}
								</div>
								<div className="jawaban"><div>A.</div><div>{soal.jawaban_1}</div></div>
								<div className="jawaban"><div>B.</div><div>{soal.jawaban_2}</div></div>
								<div className="jawaban"><div>C.</div><div>{soal.jawaban_3}</div></div>
								<div className="jawaban"><div>D.</div><div>{soal.jawaban_4}</div></div>
								<div className="jawaban"><div>E.</div><div>{soal.jawaban_5}</div></div>
								<br/>
							</div>
						))
					}
	         </div> 
        	</div>
	    </div>
	  );
	}
}


const mapStateToProps = state => ({
  	common:state.common
});

export default connect(mapStateToProps)(PrintSoal);