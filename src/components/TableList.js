import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deleteSoal, setSoal } from '../actions/soalActions';
import ModalInfo from './ModalInfo';

class TableList extends Component {
	constructor(props) {
    super(props);
	    this.state = {
	    	modal:false,
	    	idSoal: null
	    };

	   this.deleteBtn = this.deleteBtn.bind(this);
	   this.editBtn = this.editBtn.bind(this);
	   this.onClickModalHandler = this.onClickModalHandler.bind(this);
 	}

 	onClickModalHandler(idSoal){
 		const payload = !idSoal ? null:idSoal;
		this.setState({
			modal:!this.state.modal,
			idSoal: payload
		});
	}

	checkLength(text){
		const maxWord = 100;
		const newText = text.length >= maxWord ?
						text.slice(0, (maxWord-3)).concat("..."):
						text;
		return newText;
	}

	deleteBtn(idSoal){
		this.props.deleteSoal(idSoal, this.props.throw.id, this.props.throw.id_mapel,  this.props.history);
	}

	editBtn(soalData){
		this.props.history.push('/dashboard/tambah-soal')
		this.props.setSoal(soalData, this.props.history);
	}

	render() {
		const {data} = this.props;
		return (
			<div>
				<table className="cust-table table table-striped ">
			      <thead>
			        <tr>
			          <th>SOAL</th>
			          <th>Perintah</th>
			        </tr>
			      </thead>
			      <tbody>
			        {
			        	data.length == 0 ?
			        	<tr >
			          	<td colSpan="2" style={{textAlign:'center'}}>
			          		Belum ada soal
			          	</td>
			          </tr> :
			          data.map(row => (
			            <tr key={row.id_soal}>
			              	<td onClick={()=>this.props.clickSoal(row)} className='txt-modalHandler'>{this.checkLength(row.soal)}</td>
			              	<td className='act'>
			              	<input type='button' className='btn btn-secondary act-btn' onClick={() => this.editBtn(row)} value='Ubah'/>
			              	<input type='button' className='btn act-btn red-btn' onClick={() =>this.onClickModalHandler(row.id_soal)} value='Hapus'/>
			              </td>
			            </tr>
			          )) 
			          
			        }
			      </tbody>
    			</table>
		    	{
					this.state.modal &&
					<ModalInfo title='hapus' modalHandler={this.onClickModalHandler} deleteHandler={() => this.deleteBtn(this.state.idSoal)}/>
				}
			</div>
		);
	}
}

export default connect (null, {deleteSoal, setSoal})(TableList);