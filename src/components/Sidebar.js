import React, { Component } from 'react';
import './Sidebar.css';

import Avatar from './Avatar';
import Menu from './Menu';


const menuItem = [
	{"name":"Halaman Utama", "logo":"halaman-utama"},
	{"name":"Daftar Soal", "logo":"daftar-soal"},
	{"name":"Tambah Soal", "logo":"tambah-soal"},
	{"name":"Buat Soal Ujian", "logo":"buat-soal-ujian"}
]
class Sidebar extends Component {
	render() {
		return (
			<div className='sidebarWrapper'>
				<div className="sidebarAva">
					<Avatar/>
				</div>
	        	{menuItem.map((item)=> 
					<div key={item.name} className="sidebarMenu">
		        		<Menu title={item.name} logo={item.logo}/>
					</div>
	        	)}
			</div>
		);
	}
}

export default Sidebar;