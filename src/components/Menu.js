import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './Menu.css';

class Menu extends Component {
	render() {
		const {title , logo} = this.props;
		const link = logo === 'halaman-utama' ? '/studypage' : `/dashboard/${logo}`;
		return (
			<div className='menuWrapper'>
				<NavLink to={link} activeClassName="menuSelect">
				<img className='menuIcon' src={require(`../assets/${logo}.png`)} alt='menuIcon'/>
				<div className='menuTitle'>{title}</div>
				</NavLink>
			</div>
		);
	}
}

export default Menu;