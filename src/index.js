import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {globalURL} from './utils/globalSettingAxios'; 
import './index.css';

import App from './App';
// import PrintSoal from './components/PrintSoal';
import store from './store';
import registerServiceWorker from './registerServiceWorker';

// eslint-disable-next-line
const axiosSetting = globalURL;

ReactDOM.render(
	<Provider store={store}>
		<App />	
	</Provider>
	, document.getElementById('root'));
registerServiceWorker();
