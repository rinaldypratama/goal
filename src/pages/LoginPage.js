import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';

import './LoginPage.css';

import LoginForm from '../components/loginForm';
import SignupForm from '../components/signupForm';
import ModalError from '../components/ModalError';

	class LoginPage extends Component {
		componentDidMount() {
			if (this.props.user.isAuthenticated) {
				this.props.history.push('/studypage');
			}
		}

	  	componentWillReceiveProps(nextProps) {
	    	if (nextProps.user.isAuthenticated) {
	      	this.props.history.push('/studypage');
	    	}
	 	}


		render() {
			const {error} = this.props;
			const title = this.props.match.path == '/signup'? 'daftar gagal':'login gagal';
			// console.log(this.props.match.path);
			return (
				<div className="loginPage">
					<div className="loginTitle">
						<h1>Gudang Soal</h1>
					</div>
					<div className="loginBox">
						<Route exact path="/" component={LoginForm}/>
						<Route path="/signup" component={SignupForm}/>
					</div>
					{
						error &&
						<ModalError error={error} title={title}/>
					}	
				</div>
				);
	}
}

const mapStateToProps = state => ({
	user:state.user,
  	error:state.error
});

export default connect(mapStateToProps, null)(LoginPage) ;