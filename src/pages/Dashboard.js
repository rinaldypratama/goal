import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import './Dashboard.css';

import PrivateRoute from '../utils/PrivateRoute';

import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import Footer from '../components/Footer';
import TambahSoal from '../components/TambahSoal';
import DaftarSoal from '../components/DaftarSoal';
import SoalUjian from '../components/SoalUjian';
import Setting from '../components/Setting';


class Dashboard extends Component {

	
	render() {
		const { choice } = this.props.common;
		if (!choice.id_mapel) {
		    window.location.href = '/studypage';
		}
		return (
			<div className='dashboardWrapper'>
				<div className="dash-head">
					<Header/>
				</div>
				<div className="dash-sb">
					<Sidebar/>
				</div>
				<div className="dash-cont">
					<h2>{choice.mapel}</h2>
						<Route path="/dashboard/daftar-soal" component={DaftarSoal}/>
	      			<Route path="/dashboard/tambah-soal" component={TambahSoal}/>
	      			<Route path="/dashboard/buat-soal-ujian" component={SoalUjian}/>
	      			<Route path="/dashboard/setting" component={Setting}/>
				</div>
				<div className="dash-foot">
					<Footer/>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
  common: state.common
});

export default connect(mapStateToProps, null) (Dashboard);