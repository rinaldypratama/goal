import React, { Component } from "react";
import { connect } from 'react-redux';
import { getStudi, setChoice } from '../actions/commonActions'
import './StudyPage.css';

import Spinner from '../utils/Spinner';
import StudiesCard from '../components/StudiesCard';
import Footer from '../components/Footer';

class StudyPage extends Component {
	constructor(props) {
	  super(props);
	  this.ucFirst = this.ucFirst.bind(this);
	}
	//make first character uppercase
	ucFirst(string){
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
	}
	componentDidMount() {
    	this.props.getStudi();
    	// console.log("history page", this.props.history);
  	}

	render() {
		const { studi, mapel, loading } = this.props.comm;
		const { first_name, last_name } = this.props.user.user;
		return (
			<div className="studyPage">
				<div className="studyPage-header">
					<h5>"Selamat Datang, {this.ucFirst(first_name)} {this.ucFirst(last_name)}"</h5>
				</div>
				<div className="studyPage-body">
					{ loading?
						<Spinner/>:
						studi.map((data) => 
						<div key={data.id_bidang} className="study-category">
							<div className="studyName">
								<h5>{data.bidang_studi}</h5>
							</div>
							{mapel.map((d) => {
								if(d.id_bidang == data.id_bidang)	
									return <div key={d.id_mapel} className="studyContent">
												<StudiesCard studies={d} setChoice={this.props.setChoice} history={this.props.history}/>
											</div>
							}	
							)}
						</div>
						)
					}
					
				</div>
				<Footer/>
			</div>
		);
	}
}

const mapStateToProps = state => ({
  user: state.user,
  comm: state.common
});

export default connect(mapStateToProps, { getStudi, setChoice })(StudyPage);